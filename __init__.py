# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .stock import (Location, LocationDock, ShipmentIn,
    ShipmentOut, ShipmentOutReturn, ShipmentInternal,
    ShipmentInReturn)


def register():
    Pool.register(
        LocationDock,
        Location,
        ShipmentIn,
        ShipmentOut,
        ShipmentOutReturn,
        ShipmentInternal,
        ShipmentInReturn,
        module='stock_location_dock', type_='model')
