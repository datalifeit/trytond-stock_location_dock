# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class StockTestCase(ModuleTestCase):
    """Test module"""
    module = 'stock_location_dock'

    def setUp(self):
        super(StockTestCase, self).setUp()


del ModuleTestCase
